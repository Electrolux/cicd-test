module.exports = function (source) {
  //不能为箭头函数一定要这个格式,参数为引入文件的原代码
  // console.log("loader之中获取的参数",this.query)
  // 需求1：在编译阶段，把index.js里面的{{ __path__ }}转化为我们真正需要的路径
  // result=source.replace('{{ __path__ }}', 'electrolux')
  // return  result
  //this.callback 也是官方提供的API，替代return
  // this.callback(null, result); content  sourceMap
  // this.callback能传递以下四个参数.第三个参数和第四个参数可以不填.this.callback传递的参数会发送给下一个loader函数接受,每一个loader函数形成了流水线上的一道道工序,最终将代码处理成期待的结果.
  // 万一loader函数里面需要做一些异步的操作就要采用如下方式.
  //上一个loader可能会传递sourceMap和meta过来,没穿就为空
  //   module.exports = function (content, sourceMap, meta) {
  //     const callback = this.async()
  //     setTimeout(() => {
  //       // 模拟异步操作
  //       callback(null, content)
  //     }, 1000)
  //   }
  return source
}
