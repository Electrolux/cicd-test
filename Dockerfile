FROM nginx
COPY dist/ /usr/share/nginx/html/
COPY nginx/default.conf /etc/nginx/conf.d/default.conf

# docker run -p 3000:80 -d --name vueApp vuenginxcontainer
# 设置为工作目录，以下 RUN/CMD 命令都是在工作目录中进行执行  WORKDIR /code
# 安装依赖  RUN yarn
# 启动 Node Server  CMD npm start